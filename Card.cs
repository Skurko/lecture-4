﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Data
{
    class Card : IComparable, ICloneable, IXmlSerializable<Card>
    {
        private string _name;
        private long _synCode;
        private long _branchId;
        private List<Contact> _contacts = new List<Contact>();
        private CardStatus _cardStatus = CardStatus.NotSet;

        public List<Contact> Contacts
        {
            get { return _contacts; }
        }

        public CardStatus CardStatus
        {
            get { return _cardStatus; }
            set { _cardStatus = value; }
        }

        public Card() { }

        public Card(string name, long synCode, long branchId)
        {
            if ((name == "") || (branchId < 0))
            {
                throw new ArgumentException();
            }

            _name = name;
            _synCode = synCode;
            _branchId = branchId;
        }

        public void SetCardStatus(CardStatus cardStatus)
        {
            CardStatus = cardStatus;
        }

        public void ChangeName(string name)
        {
            if (name == "")
            {
                throw new ArgumentException("Имя не может быть пустым");
            }

            _name = name;
        }

        public void ChangeId(long id)
        {
            if (id < 0)
            {
                throw new ArgumentException("BranchId не может быть отрицательным");
            }

            _branchId = id;
        }

        public void AddContact(Contact contact)
        {
            _contacts.Add(contact);
        }

        public void PrintAllContacts()
        {
            foreach (var contact in _contacts)
            {
                Console.WriteLine(contact.GetType().ToString().Remove(0, 5) + ": " + contact.ToString());
            }
        }

        public void DeleteContactByValue(string value)
        {
            _contacts.RemoveAll(a => a.Value == value);
        }



        public int CompareTo(object obj)
        {
            var card = (Card) obj;
            if ((this._name == card._name) && (this._synCode == card._synCode) && (this._branchId == card._branchId))
            {
                return 0;
            }
            else if (this._synCode < card._synCode)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }

        public object Clone()
        {
            var card = new Card(_name, _synCode, _branchId);
            _contacts.ForEach(c => card.AddContact(c));
            return card;
        }

        public XElement ToXml()
        {
            var element = new XElement("Card");
            element.Add(new XAttribute("Name", _name));
            element.Add(new XAttribute("Syncode", _synCode));
            element.Add(new XAttribute("BranchId", _branchId));
            element.Add(new XAttribute("Status", (int)_cardStatus));
            element.Add(new XElement("Contacts"));
            _contacts.ForEach(c => element.Element("Contacts").Add(c.ToXml()));
            return element;
        }

        public XDocument SaveToXml()
        {
            var doc = new XDocument();
            doc.Add(new XElement(ToXml()));
            return doc;
        }

        public Card LoadFromXml(XDocument doc)
        {
            var name = doc.Element("Card").Attribute("Name").Value;
            var syncode = Convert.ToInt64(doc.Element("Card").Attribute("Syncode").Value);
            var branchId = Convert.ToInt64(doc.Element("Card").Attribute("BranchId").Value); ;
            var status = (CardStatus) Convert.ToInt32(doc.Element("Card").Attribute("Status").Value); ;
            this._name = name;
            this._synCode = syncode;
            this._branchId = branchId;
            this.CardStatus = status;

            foreach (var element in doc.Element("Card").Element("Contacts").Elements())
            {
                if (element.Name == "Email")
                {
                    this.AddContact(new EmailContact().LoadFromXml(new XDocument(element)));
                }
                else
                {
                    this.AddContact(new PhoneContact().LoadFromXml(new XDocument(element)));
                }
            }
            return this;
        }
    }
}

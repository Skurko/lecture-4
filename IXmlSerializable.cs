﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Data
{
    interface IXmlSerializable<T>
    {
        XDocument SaveToXml();

        T LoadFromXml(XDocument doc);
    }
}

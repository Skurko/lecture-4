﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Data
{
    class PhoneContact : Contact
    {
        public string Zone;

        public PhoneContact() { }

        public PhoneContact(string zone, string value)
        {
            Zone = zone;
            Value = value;
        }

        public override string ToString()
        {
            return "(" + Zone + ") " + Value;
        }

        public override XElement ToXml()
        {
            var element = new XElement("Phone");
            element.Add(new XAttribute("Value", Value));
            element.Add(new XAttribute("Zone", Zone));
            return element;
        }

        public override bool Equals(Contact other)
        {
            return (this.CompareTo(other) == 0);
        }

        public override XDocument SaveToXml()
        {
            var doc = new XDocument();
            doc.Add(new XElement(ToXml()));
            return doc;
        }

        public override Contact LoadFromXml(XDocument doc)
        {
            var value = doc.Element("Phone").Attribute("Value").Value;
            var zone = doc.Element("Phone").Attribute("Zone").Value;
            this.Zone = zone;
            this.Value = value;
            return this;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Data
{
    class EmailContact : Contact
    {
        public string Alias;

        public EmailContact() { }

        public EmailContact(string value, string alias)
        {
            Value = value;
            Alias = alias;
        }

        public override string ToString()
        {
            return "mailto:" + Value + " (" + Alias + ")";
        }

        public override XElement ToXml()
        {
            var element = new XElement("Email");
            element.Add(new XAttribute("Value", Value));
            element.Add(new XAttribute("Alias", Alias));
            return element;
        }

        public override bool Equals(Contact other)
        {
            return (this.CompareTo(other) == 0);
        }

        public override XDocument SaveToXml()
        {
            var doc = new XDocument();
            doc.Add(new XElement(ToXml()));
            return doc;
        }

        public  override Contact LoadFromXml(XDocument doc)
        {
            var value = doc.Element("Email").Attribute("Value").Value;
            var alias = doc.Element("Email").Attribute("Alias").Value;
            this.Value = value;
            this.Alias = alias;
            return this;
        }
    }
}

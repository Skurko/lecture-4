﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    enum CardStatus
    {
        NotSet = 0,
        Verified = 1,
        Assigned = 100,
        Archived = 2000
    }
}

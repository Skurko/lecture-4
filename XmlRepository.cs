﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Data
{
    class XmlRepository
    {
        public void SaveToXml<T>(T obj, string name) where T : IXmlSerializable<T>
        {
            var doc = obj.SaveToXml();

            doc.Save(name);
        }

        public T LoadFromXml<T>(string path) where T : IXmlSerializable<T>, new()
        {
            var doc = XDocument.Load(path);
            var obj = new T();
            obj.LoadFromXml(doc);
            return obj;
        }
    }
}

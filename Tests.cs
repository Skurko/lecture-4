﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Data
{
    [TestFixture]
    public class Tests
    {
        private readonly string _email = "test@test.test";
        private readonly string _alias = "Test Email";
        private readonly string _zone = "666";
        private readonly string _value = "6-666-666";

        [Test]
        public void EmailContactTest()
        {
            var emailContact = new EmailContact(_email, _alias);

            Assert.That(emailContact.Value, Is.EqualTo(_email), "Неверное значение свойства Value");
            Assert.That(emailContact.Alias, Is.EqualTo(_alias), "Неверное значение свойства Alias");
        }

        [Test]
        public void EmailToStringTest()
        {
            var emailContact = new EmailContact(_email, _alias);
            var expectedString = "mailto:" + _email + " (" + _alias + ")";
            
            Assert.That(emailContact.ToString(), Is.EqualTo(expectedString), "Неверный результат работы метода ToString");
        }

        [Test]
        public void EmailToXmlTest()
        {
            var emailContact = new EmailContact(_email, _alias);
            var xml = emailContact.ToXml();

            Assert.That(xml.HasAttributes, Is.EqualTo(true), "Элемент должен иметь атрибуты");
            Assert.That(xml.HasElements, Is.EqualTo(false), "Не должно быть дочерних элементов");
            Assert.That(xml.Attributes().Count(), Is.EqualTo(2), "Должно быть 2 атрибута");
            Assert.That(xml.Attribute("Value"), Is.Not.Null, "Должен существовать атрибут Value");
            Assert.That(xml.Attribute("Alias"), Is.Not.Null, "Должен существовать атрибут Alias");
            Assert.That(xml.Attribute("Value").Value, Is.EqualTo(_email), "Значение атрибута Value должно быть равно " + _email);
            Assert.That(xml.Attribute("Alias").Value, Is.EqualTo(_alias), "Значение атрибута Alias должно быть равно " + _alias);
        }

        [Test]
        public void EmailCompareToTest()
        {
            var emailContact1 = new EmailContact(_email, _alias);
            var emailContact2 = new EmailContact(_email, _alias);
            Assert.That(emailContact1.CompareTo(emailContact2), Is.EqualTo(0), "Результат сравнения должен быть 0");

            emailContact2 = new EmailContact(_email, _alias + "1");
            Assert.That(emailContact1.CompareTo(emailContact2), Is.EqualTo(0), "Результат сравнения должен быть 0");

            emailContact2 = new EmailContact(_email, _alias.Remove(_alias.Length - 1));
            Assert.That(emailContact1.CompareTo(emailContact2), Is.EqualTo(0), "Результат сравнения должен быть 0");

            emailContact2 = new EmailContact(_email + "1", _alias);
            Assert.That(emailContact1.CompareTo(emailContact2), Is.EqualTo(-1), "Результат сравнения должен быть -1");

            emailContact2 = new EmailContact(_email.Remove(_email.Length - 1), _alias);
            Assert.That(emailContact1.CompareTo(emailContact2), Is.EqualTo(1), "Результат сравнения должен быть 1");
        }

        [Test]
        public void PhoneContactTest()
        {
            var phoneContact = new PhoneContact(_zone, _value);

            Assert.That(phoneContact.Zone, Is.EqualTo(_zone), "Неверное значение свойства Zone");
            Assert.That(phoneContact.Value, Is.EqualTo(_value), "Неверное значение свойства Value");
        }

        [Test]
        public void PhoneToStringTest()
        {
            var phoneContact = new PhoneContact(_zone, _value);
            var expectedString = "(" + _zone + ") " + _value;

            Assert.That(phoneContact.ToString(), Is.EqualTo(expectedString), "Неверный результат работы метода ToString");
        }

        [Test]
        public void PhoneToXmlTest()
        {
            var phoneContact = new PhoneContact(_zone, _value);
            var xml = phoneContact.ToXml();

            Assert.That(xml.HasAttributes, Is.EqualTo(true), "Элемент должен иметь атрибуты");
            Assert.That(xml.HasElements, Is.EqualTo(false), "Не должно быть дочерних элементов");
            Assert.That(xml.Attributes().Count(), Is.EqualTo(2), "Должно быть 2 атрибута");
            Assert.That(xml.Attribute("Zone"), Is.Not.Null, "Должен существовать атрибут Zone");
            Assert.That(xml.Attribute("Value"), Is.Not.Null, "Должен существовать атрибут Value");
            Assert.That(xml.Attribute("Zone").Value, Is.EqualTo(_zone), "Значение атрибута Zone должно быть равно " + _zone);
            Assert.That(xml.Attribute("Value").Value, Is.EqualTo(_value), "Значение атрибута Value должно быть равно " + _value);
        }

        [Test]
        public void PhoneCompareTest()
        {
            var phoneContact1 = new PhoneContact(_zone, _value);
            var phoneContact2 = new PhoneContact(_zone, _value);
            Assert.That(phoneContact1.CompareTo(phoneContact2), Is.EqualTo(0), "Результат сравнения должен быть 0");

            phoneContact2 = new PhoneContact(_zone, _value + "1");
            Assert.That(phoneContact1.CompareTo(phoneContact2), Is.EqualTo(-1), "Результат сравнения должен быть -1");

            phoneContact2 = new PhoneContact(_zone, _value.Remove(_value.Length - 1));
            Assert.That(phoneContact1.CompareTo(phoneContact2), Is.EqualTo(1), "Результат сравнения должен быть 1");

            phoneContact2 = new PhoneContact(_zone + "1", _value);
            Assert.That(phoneContact1.CompareTo(phoneContact2), Is.EqualTo(0), "Результат сравнения должен быть 0");

            phoneContact2 = new PhoneContact(_zone.Remove(_zone.Length - 1), _value);
            Assert.That(phoneContact1.CompareTo(phoneContact2), Is.EqualTo(0), "Результат сравнения должен быть 0");
        }

        [Test]
        public void CardTest()
        {
            Card card;
            Assert.Throws<ArgumentException>(() => card = new Card("", 123, 123), "Должно быть брошено исключение ArgumentException");
            Assert.Throws<ArgumentException>(() => card = new Card("Test Card", 123, -123), "Должно быть брошено исключение ArgumentException");
            Assert.Throws<ArgumentException>(() => card = new Card("", 123, -123), "Должно быть брошено исключение ArgumentException");
            Assert.DoesNotThrow(() => card = new Card("Test Card", 123, 123), "Исключение не должно быть брошено");
            Assert.DoesNotThrow(() => card = new Card(), "Исключение не должно быть брошено");
        }

        [Test]
        public void CardChangeNameTest()
        {
            var card = new Card("Test Card", 123, 123);

            Assert.Throws<ArgumentException>(() => card.ChangeName(""), "Должно быть брошено исключение ArgumentException");
            Assert.DoesNotThrow(() => card.ChangeName("New Name"), "Исключение не должно быть брошено");
        }

        [Test]
        public void CardChangeIdTest()
        {
            var card = new Card("Test Card", 123, 123);

            Assert.Throws<ArgumentException>(() => card.ChangeId(-123), "Должно быть брошено исключение ArgumentException");
            Assert.DoesNotThrow(() => card.ChangeId(1), "Исключение не должно быть брошено");
        }

        [Test]
        public void CardContactsTest()
        {
            var card = new Card("Test Card", 123, 123);
            var contacts = card.Contacts;
            Assert.That(contacts, Is.EqualTo(card.Contacts));

            card.AddContact(new PhoneContact(_zone, _value));
            Assert.That(contacts, Is.EqualTo(card.Contacts));
        }
        
        [Test]
        public void CardAddContact()
        {
            var card = new Card("Test Card", 123, 123);
            var numOfContacts = card.Contacts.Count;

            card.AddContact(new PhoneContact(_zone, _value));
            numOfContacts++;
            Assert.That(numOfContacts, Is.EqualTo(card.Contacts.Count));
            card.AddContact(new EmailContact(_email, _alias));
            numOfContacts++;
            Assert.That(numOfContacts, Is.EqualTo(card.Contacts.Count));
        }

        [Test]
        public void CardDeleteContactByValueTest()
        {
            var card = new Card("Test Card", 123, 123);
            card.AddContact(new PhoneContact(_zone, _value));
            card.AddContact(new EmailContact(_email, _alias));
            var numOfContacts = card.Contacts.Count;
            var a = card.Contacts.Contains(new PhoneContact(_zone, _value));
            Assert.That(card.Contacts.Contains(new PhoneContact(_zone, _value)), Is.EqualTo(true));
            Assert.That(card.Contacts.Contains(new EmailContact(_email, _alias)), Is.EqualTo(true));
            Assert.That(numOfContacts, Is.EqualTo(2));

            card.DeleteContactByValue(_value);
            numOfContacts--;
            Assert.That(card.Contacts.Contains(new PhoneContact(_zone, _value)), Is.EqualTo(false));
            Assert.That(numOfContacts, Is.EqualTo(card.Contacts.Count));
            card.DeleteContactByValue(_email);
            numOfContacts--;
            Assert.That(card.Contacts.Contains(new EmailContact(_email, _alias)), Is.EqualTo(false));
            Assert.That(numOfContacts, Is.EqualTo(card.Contacts.Count));
        }

        [Test]
        public void CardCloneTest()
        {
            var card = new Card("Test Card", 123, 123);
            var clonedCard = (Card) card.Clone();

            Assert.That(card.CompareTo(clonedCard), Is.EqualTo(0));

            clonedCard.ChangeId(1);
            Assert.That(card.CompareTo(clonedCard), Is.EqualTo(1));
        }

        [Test]
        public void CardToXmlTest()
        {
            var card = new Card("Test Card", 123, 123);
            card.AddContact(new PhoneContact(_zone, _value));
            card.AddContact(new EmailContact(_email, _alias));
            var xml = card.ToXml();

            Assert.That(xml.HasAttributes, Is.EqualTo(true), "Элемент должен иметь атрибуты");
            Assert.That(xml.HasElements, Is.EqualTo(true), "Элемент должен иметь дочерние элементы");
            Assert.That(xml.Attributes().Count(), Is.EqualTo(4), "Должно быть 4 атрибута");
            Assert.That(xml.Elements().Count(), Is.EqualTo(1), "Должен быть 1 дочерний элемент");
            Assert.That(xml.Element("Contacts").Elements().Count(), Is.EqualTo(card.Contacts.Count), "Должно быть " + card.Contacts.Count + " дочерних элементов");
            Assert.That(xml.Attribute("Name"), Is.Not.Null, "Должен существовать атрибут Name");
            Assert.That(xml.Attribute("Syncode"), Is.Not.Null, "Должен существовать атрибут Syncode");
            Assert.That(xml.Attribute("BranchId"), Is.Not.Null, "Должен существовать атрибут BranchId");
            Assert.That(xml.Attribute("Status"), Is.Not.Null, "Должен существовать атрибут Status");
            Assert.That(xml.Attribute("Name").Value, Is.EqualTo("Test Card"), "Значение атрибута Zone должно быть равно Test Card");
            Assert.That(Convert.ToInt64(xml.Attribute("Syncode").Value), Is.EqualTo(123), "Значение атрибута Value должно быть равно 123");
            Assert.That(Convert.ToInt64(xml.Attribute("BranchId").Value), Is.EqualTo(123), "Значение атрибута Value должно быть равно 123");
            Assert.That(Convert.ToInt32(xml.Attribute("Status").Value), Is.EqualTo((int)CardStatus.NotSet), "Значение атрибута Value должно быть равно NotSet");
        }
    }
}

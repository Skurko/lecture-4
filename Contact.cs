﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Data
{
    abstract class Contact : IComparable, IEquatable<Contact>, IXmlSerializable<Contact>
    {
        public string Value;

        public int CompareTo(object obj)
        {
            var contact = (Contact) obj;
            return String.Compare(this.Value, contact.Value);
        }

        public abstract XElement ToXml();

        public abstract bool Equals(Contact other);

        public abstract XDocument SaveToXml();

        public abstract Contact LoadFromXml(XDocument doc);
    }
}

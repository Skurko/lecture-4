﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Data
{
    class Program
    {
        private static void Lecture8()
        {
            var card = new Card("TestCard", 100, 1);
            
            card.AddContact(new PhoneContact("383", "355-10-10"));
            card.AddContact(new PhoneContact("384-53", "355-10-10"));
            card.AddContact(new PhoneContact("951", "363-41-58"));
            card.AddContact(new EmailContact("test1@gmail.com", "Google"));
            card.AddContact(new EmailContact("test2@я.рф", "Первый"));
            card.AddContact(new EmailContact("тест@я.рф", "Второй"));
            card.AddContact(new EmailContact("test2@ya.ru", "Yandex"));//dont change
            card.AddContact(new EmailContact("test3@ya.ru", "Яндекс"));
            card.AddContact(new EmailContact("тест@ya.ru", "Yandex"));
            card.AddContact(new EmailContact("test4@кириллический.домен", "Третий"));

            Console.WriteLine("Все контакты:");
            card.PrintAllContacts();
            Console.WriteLine("-------------------------------");

            //Из списка контактов выбрать все контакты типа Email, в кириллическом домене
            var list = card.Contacts.OfType<EmailContact>().Where(c => Regex.IsMatch(c.Value.Substring(c.Value.IndexOf("@") + 1), "^[А-Яа-я.]+$") == true).ToList();
            var oldCard = new Card("Old Card", 1, 1);
            list.ForEach(c => oldCard.AddContact(new EmailContact(c.Value, c.Alias)));
            oldCard.PrintAllContacts();
            Console.ReadKey();
            Console.WriteLine("-------------------------------");

            //Из списка контактов выбрать все телефоны, отсортировать в алфавитном порядке
            card.AddContact(new PhoneContact("384-53", "155-10-10"));
            card.AddContact(new PhoneContact("384-53", "655-10-10"));
            card.AddContact(new PhoneContact("384-53", "355-11-10"));
            card.AddContact(new PhoneContact("384-53", "955-10-10"));
            card.AddContact(new PhoneContact("384-53", "055-10-10"));

            card.PrintAllContacts();
            Console.WriteLine("-------------------------------");
            var list2 = card.Contacts.OfType<PhoneContact>().ToList();
            list2 = list2.OrderBy(c => c.Value).ToList();
            oldCard = new Card("Old Card", 1, 1);
            list2.ForEach(c => oldCard.AddContact(new PhoneContact(c.Zone, c.Value)));
            Console.WriteLine("Телефоны отсортированные по алфавиту");
            oldCard.PrintAllContacts();
            Console.ReadKey();
        }

        private static void Lecture9()
        {
            //сохраняем xml
            var card = new Card("TestCard", 100, 1);
            card.AddContact(new PhoneContact("383", "355-10-10"));
            card.AddContact(new PhoneContact("384-53", "355-10-10"));
            card.AddContact(new PhoneContact("951", "363-41-58"));
            card.AddContact(new EmailContact("test1@gmail.com", "Google"));

            var repo = new XmlRepository();
            repo.SaveToXml(card, "test2.xml");

            //читаем из xml
            var card2 = repo.LoadFromXml<Card>("test.xml");
            card2.PrintAllContacts();
        }

        private static void Main(string[] args)
        {
            //Lecture8();
            Lecture9();

            

            /*
            var card = new Card("TestCard", 100, 1);
            var contact11 = new PhoneContact("383", "355-10-10");
            var contact12 = new PhoneContact("384-53", "355-10-10");
            var contact13 = new PhoneContact("951", "363-41-58");
            var contact14 = new EmailContact("test1@gmail.com", "Google");
            var contact15 = new EmailContact("test2@ya.ru", "Yandex");

            card.AddContact(contact11);
            card.AddContact(contact12);
            card.AddContact(contact13);
            card.AddContact(contact14);
            card.AddContact(contact15);

            Console.WriteLine("Все контакты:");
            card.PrintAllContacts();
            
            Console.WriteLine(card.ToXml());
            
            var contact1 = new PhoneContact("383", "1234567");
            var contact2 = new EmailContact("test1@test1.ru","test1");
            var contact3 = new EmailContact("test2@test2.ru","test2");

            var contactsList = new List<Contact>();
            contactsList.Add(contact1);
            contactsList.Add(contact2);
            contactsList.Add(contact3);

            using (StreamWriter sw = new StreamWriter("contacts.txt"))
            {
                contactsList.ForEach(c => sw.WriteLine(c.Value));
            }

            string text;
            using (StreamReader sr = new StreamReader("contacts.txt"))
            {
                text = sr.ReadToEnd();
            }

            Console.WriteLine(text);
            Console.ReadKey();
            */
            /*
            var card = new Card("TestCard", 100, 1);
            var contact1 = new PhoneContact("383", "355-10-10");
            var contact2 = new PhoneContact("384-53", "355-10-10");
            var contact3 = new PhoneContact("951", "363-41-58");
            var contact4 = new EmailContact("test1@gmail.com", "Google");
            var contact5 = new EmailContact("test2@ya.ru", "Yandex");

            card.AddContact(contact1);
            card.AddContact(contact2);
            card.AddContact(contact3);
            card.AddContact(contact4);
            card.AddContact(contact5);

            Console.WriteLine("Все контакты:");
            card.PrintAllContacts();
            Console.WriteLine("\nУдаляем некоторые контакты:");
            card.DeleteContactByValue("test1@gmail.com");
            card.DeleteContactByValue("355-10-10");
            card.PrintAllContacts();
            
            var cardsList = new List<Card>();
            cardsList.Add(card);

            while (true)
            {
                Console.WriteLine(
                    "\nНажмите Esc для редактирования текущей карточки, F1 для создания новой карточки или другую кнопку для выхода");
                var key = Console.ReadKey(true);
                if ((key.Key != ConsoleKey.Escape) && (key.Key != ConsoleKey.F1))
                {
                    break;
                }

                if (key.Key == ConsoleKey.Escape)
                {
                    while (true)
                    {
                        Console.WriteLine(
                            "\nНажмите Esc для добавления нового телфонного контакты, F1 для добавления email или другую кнопку для выхода");
                        var cki = Console.ReadKey(true);
                        if ((cki.Key != ConsoleKey.Escape) && (cki.Key != ConsoleKey.F1))
                        {
                            break;
                        }

                        var param1 = Console.ReadLine();
                        var param2 = Console.ReadLine();

                        if (cki.Key == ConsoleKey.Escape)
                        {
                            card.AddContact(new PhoneContact(param1, param2));
                        }
                        else
                        {
                            card.AddContact(new EmailContact(param1, param2));
                        }

                        Console.WriteLine("\nВсе контакты карточки:");
                        card.PrintAllContacts();
                    }
                }
                else
                {
                    var param1 = Console.ReadLine();
                    var param2 = Console.ReadLine();
                    var param3 = Console.ReadLine();

                    try
                    {
                        card = new Card(param1, Convert.ToInt64(param2), Convert.ToInt64(param3));
                        cardsList.Add(card);
                    }
                    catch (Exception e)
                    {
                        
                        Console.WriteLine(e.ToString());
                    }
                    
                }

            }*/
        }
    }
}
